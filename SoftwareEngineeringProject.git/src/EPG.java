import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class EPG {
	public static void main(String[] args) throws IOException, InterruptedException {
		Scanner userInput = new Scanner(System.in);
		ArrayList<Channel> channelList = new ArrayList<Channel>(); // list of channels in the epg
		ArrayList<Programme> recordingsList = new ArrayList<Programme>(); // schedule of programs
		boolean stop = false;
		System.out.println("How many tuners does your system have?");
		int tunersLeft = userInput.nextInt();
		System.out.println("Please enter a parental password to access the EPG");
		String password = userInput.next();
		readTheReferenceFile(channelList);
		collectRecordings(recordingsList, channelList, tunersLeft);
		while (!stop) {
			stop = mainMenu(userInput, recordingsList, channelList, stop, tunersLeft, password);
		}
	}

	/**
	 * Removes programs which cross over the threshold of 2 days, thus making them
	 * duplicates
	 * 
	 * @param channelList list of all of the stored channels in the EPG
	 */
	public static void wrapTimes(ArrayList<Channel> channelList) {
		for (Channel ch : channelList) {
			Date chanDate = ch.getDate();
			String channelName = ch.getId();
			boolean followingDay = false;
			ArrayList<Programme> progToRemove = new ArrayList<>();
			for (Programme prog : ch.getProgramme()) {
				prog.date = chanDate;
				prog.channel = channelName;
				if (prog.intersectsTime(2359)) {
					Calendar c = Calendar.getInstance();
					c.setTime(chanDate);
					c.add(Calendar.DAY_OF_YEAR, 1);
					chanDate = c.getTime();
					followingDay = true;
				}
				if (followingDay) {
					progToRemove.add(prog); // remove this programme
				}
			}
			ch.getProgramme().removeAll(progToRemove);
		}
		System.out.println();
	}

	/**
	 * Reads the reference file, which stored the separate parts of the XML web
	 * pages needed to create the EPG
	 * 
	 * @param channelList list of all of the stored channels in the EPG
	 * @throws IOException          Exception regarding the reference file, such as
	 *                              whether it exists, or is in the correct location
	 * @throws InterruptedException Exception regarding the thread.sleep
	 */
	public static void readTheReferenceFile(ArrayList<Channel> channelList) throws IOException, InterruptedException {
		String fileName = ("referenceFile.txt"); // file which will be used to
		// collect web data
		String webAddress;
		String writeBackLocation;
		FileReader reference = new FileReader(fileName);
		Scanner read = new Scanner(reference);
		final int startDay = Integer.parseInt(read.nextLine());
		final int endDay = Integer.parseInt(read.nextLine());
		final String sourceAddress = read.nextLine();
		while (read.hasNext()) {
			String show = read.nextLine();
			for (int j = startDay; j <= endDay; j++) {
				webAddress = (sourceAddress + j + "/" + show + ".xml");
				String parentDir = ("EPGFolder/" + j + "/");
				writeBackLocation = (parentDir + show + ".xml");
				File parentDirFile = new File(parentDir);
				parentDirFile.mkdirs();
				File programXMLData = new File(writeBackLocation);
				collectXMLData(webAddress, writeBackLocation);
				createProgAndChan(writeBackLocation, channelList);
				Thread.sleep(2000);
			}
		}
		read.close();
		wrapTimes(channelList);
	}

	/**
	 * collect the XML data from the given web address
	 * 
	 * @param webAddress        web address, from which the data will be collected
	 * @param writeBackLocation location which the data will be written back to and
	 *                          stored in when the data has been collected
	 * @throws IOException Exception for file channel, writing the collected data
	 *                     into the writeback location file
	 */
	public static void collectXMLData(String webAddress, String writeBackLocation) throws IOException {
		ReadableByteChannel readChannel = Channels.newChannel(new URL(webAddress).openStream());
		FileOutputStream fileOS = new FileOutputStream(writeBackLocation);
		FileChannel writeChannel = fileOS.getChannel();
		writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);
		fileOS.close();
	}

	/**
	 * Using JAXB, creates and stores the channels, and their associated programs,
	 * and adding them to a channel list
	 * 
	 * @param writeBackLocation location in which the XML data is stored
	 * @param channelList       list of all the channels
	 */
	public static void createProgAndChan(String writeBackLocation, ArrayList<Channel> channelList) {
		try {
			File programData = new File(writeBackLocation);
			JAXBContext jaxbContext = JAXBContext.newInstance(Channel.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Channel channel = (Channel) jaxbUnmarshaller.unmarshal(programData);
			System.out.println(
					"Downloaded: " + channel.id + " " + SimpleDateFormat.getDateInstance().format(channel.date));
			channelList.add(channel);
			System.out.println();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Main menu, from which the user can access the two main parts of the EPG
	 * 
	 * @param userInput      The user's input
	 * @param recordingsList List of all the programs that the user wants to record
	 * @param channelList    List of all the channels
	 * @param stop           Value which, when changed, stops the program from
	 *                       running
	 * @param tuners         Number of tuners, signifying the number of programs
	 *                       which can be recorded simultaneously
	 * @return returns value which signifies if the program should continue
	 * @throws IOException Exception regarding writing the recordings to a file
	 */
	public static boolean mainMenu(Scanner userInput, ArrayList<Programme> recordingsList,
			ArrayList<Channel> channelList, boolean stop, int tuners, String password) throws IOException {
		boolean valid = false;
		DateTimeFormatter tf = DateTimeFormatter.ofPattern("HHmm");
		DateTimeFormatter df = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
		while (!valid) {
			System.out.println("|Main Menu         |");
			System.out.println("|EPG (E)           |");
			System.out.println("|My recordings (R) |");
			System.out.println("|Leave EPG (L)     |");
			char choice = userInput.next().toUpperCase().charAt(0);
			switch (choice) {
			case 'E':
				EPGLayout(tf, userInput, channelList, recordingsList, tuners, df, password);
				valid = true;
				break;
			case 'R':
				recordingMenu(channelList, userInput, recordingsList, tuners);
				valid = true;
				break;
			case 'L':
				writeRecordings(recordingsList);
				stop = true;
				valid = true;
				break;
			default:
				System.out.println("Please enter either E, R or L");
				break;
			}
		}
		return stop;
	}

	/**
	 * method used to allow the user to search for a program via an entered key word
	 * 
	 * @param channelList    List of all the channels
	 * @param userInput      The user's input
	 * @param recordingsList List of all the programs that the user wants to record
	 * @param tuners         Number of tuners, signifying the number of programs
	 *                       which can be recorded simultaneously
	 */
	public static void searchEPG(ArrayList<Channel> channelList, Scanner userInput, ArrayList<Programme> recordingsList,
			int tuners, String password) {
		System.out.println();
		System.out.println("Please enter the key word that you would like to search for:");
		String keyWord = userInput.next();
		for (Channel chan : channelList) {
			for (Programme prog : chan.programme) {
				if (prog.title.toUpperCase().contains(keyWord.toUpperCase())) {
					System.out.println(chan.id + " : " + prog.title + ": " + String.format("%04d", prog.start) + "--"
							+ String.format("%04d", prog.end) + " ("
							+ SimpleDateFormat.getDateInstance().format(prog.date) + ")");
				}
			}
		}
		recordingChoice(channelList, recordingsList, userInput, keyWord, tuners, password);
	}

	/**
	 * Allows the user to chose whether or not they would like to record a program,
	 * based on the what they just searched for
	 * 
	 * @param channelList    List of all the channels
	 * @param recordingsList List of all the programs that the user wants to record
	 * @param userInput      The user's input
	 * @param keyWord        The key word which the user has searched for
	 * @param tunersLeft     The number of tuners which the user's system has
	 */
	private static void recordingChoice(ArrayList<Channel> channelList, ArrayList<Programme> recordingsList,
			Scanner userInput, String keyWord, int tunersLeft, String password) {
		boolean valid = false;
		System.out.println("Would you like to record this ? (Y|N)");
		char answer = userInput.next().toUpperCase().charAt(0);
		switch (answer) {
		case 'Y':
			System.out.println("On which day? (dd-mmm-yyyy)");
			String date = userInput.next();
			System.out.println("At what time? (HHmm)");
			String timeOfProg = userInput.next();
			for (Channel chan : channelList) {
				for (Programme prog : chan.programme) {
					if ((prog.title.toUpperCase().contains(keyWord.toUpperCase()))
							&& (date.equals(SimpleDateFormat.getDateInstance().format(chan.date)))
							&& (timeOfProg.equals(String.format("%04d", prog.start)))) {
						if (Integer.parseInt(timeOfProg) >= 2100) {
							while (!valid) {
								System.out.println(
										"Please enter password to be allowed to record a program after the watershed hour");
								String entry = userInput.next();
								if (entry.equals(password)) {
									valid = true;
								} else {
									System.out.println("Please enter the correct password");
								}
							}
						}
						boolean isClash = recordingClash(prog, tunersLeft, recordingsList);
						if ((!recordingsList.contains(prog)) && (!isClash)) {
							recordingsList.add(prog);
							recordingMenu(channelList, userInput, recordingsList, tunersLeft);
						} else if (recordingsList.contains(prog)) {
							ArrayList<Programme> progToRemove = new ArrayList<>();
							System.out.println(
									"This program is already being recorded. Do you want to unrecord it? (Y/N)");
							char unrecord = userInput.next().toUpperCase().charAt(0);
							switch (unrecord) {
							case 'Y':
								progToRemove.add(prog);
								System.out.println("Program unrecorded");
								break;
							default:
								System.out.println("Returning to main menu");
								break;
							}
							if (!progToRemove.isEmpty()) {
								recordingsList.removeAll(progToRemove);
								progToRemove.clear();
							}
						} else if (isClash) {
							if (tunersLeft - 1 != 0) {
								tunersLeft -= 1;
								recordingsList.add(prog);
								recordingMenu(channelList, userInput, recordingsList, tunersLeft);
							} else {
								alternativeShowing(prog, recordingsList, channelList, userInput);
								tunersLeft -= 1;
							}
						}
					}
				}
			}
			break;
		default:
			System.out.println("Returning to menu");
			break;
		}
	}

	/**
	 * Provides, if there is one, an alternate showing of the program which the user
	 * is attempting to record, if the number of tuners will not allow them to
	 * record the one the selected already
	 * 
	 * @param prog           The program the user wants to record
	 * @param recordingsList List of all the programs that the user wants to record
	 * @param channelList    List of all the channels
	 * @param userInput      The user's input
	 */
	public static void alternativeShowing(Programme prog, ArrayList<Programme> recordingsList,
			ArrayList<Channel> channelList, Scanner userInput) {
		boolean otherShowing = false;
		for (Channel chan : channelList) {
			for (Programme program : chan.programme) {
				if ((program.desc.equals(prog.desc)) && (program.channel.equals(prog.channel))
						&& (program.title.equals(prog.title))) {
					if ((program.date.equals(prog.date) && (program.start != prog.start))
							|| (!(program.date.equals(prog.date)) && (program.start == prog.start))) {
						otherShowing = true;
						System.out.println(prog.channel + " : " + prog.title + ": " + String.format("%04d", prog.start)
								+ "--" + String.format("%04d", prog.end) + " ("
								+ SimpleDateFormat.getDateInstance().format(prog.date) + ")");
					}
				}
			}
		}
		if (otherShowing) {
			System.out.println("Would you like to record the new occurance of your program? (Y|N)");
			char answer = userInput.next().toUpperCase().charAt(0);
			switch (answer) {
			case 'Y':
				System.out.println("On which day? (dd-mmm-yyyy)");
				String date = userInput.next();
				System.out.println("At what start time? (HHmm)");
				String timeOfProg = userInput.next();
				for (Channel channel : channelList) {
					for (Programme newProgram : channel.programme) {
						if (newProgram.title.equals(prog.title) && newProgram.desc.equals(prog.desc)
								&& newProgram.channel.equals(prog.channel)
								&& date.contentEquals(SimpleDateFormat.getDateInstance().format(prog.date))
								&& timeOfProg.equals(String.format("%04d", newProgram.start))) {
							recordingsList.add(prog);
						}
					}
				}
			}
		} else {
			System.out.println("There are no alternative showings");
		}
	}

	/**
	 * Function used to check if there is a recording clash of the program the user
	 * is choosing to record, and the ones they are recording already
	 * 
	 * @param prog           The program the user wants to record
	 * @param tunersLeft     The number of tuners which the user's system has
	 * @param recordingsList List of all the programs that the user wants to record
	 * @return
	 */
	public static boolean recordingClash(Programme prog, int tunersLeft, ArrayList<Programme> recordingsList) {
		boolean clash = false;
		for (Programme program : recordingsList) {
			if ((program.start <= prog.start) && (program.end >= prog.start)) {
				clash = true;
			}
		}
		return clash;
	}

	/**
	 * Displays the schedule of the programs the user has chosen to record
	 * 
	 * @param channelList    The list of all the available channels
	 * @param userInput      The user's input
	 * @param recordingsList List of all the programs that the user wants to record
	 * @param tunersLeft     The number of tuners which the user's system has
	 */
	public static void recordingMenu(ArrayList<Channel> channelList, Scanner userInput,
			ArrayList<Programme> recordingsList, int tunersLeft) {
		System.out.println("_______________________________________________________________________");
		System.out.println("|      Program      |    Channel    |      Time      |      Date      |");
		System.out.println("|-------------------|---------------|----------------|----------------|");
		showRecordings(recordingsList);
		System.out.println("-------------------------------------------------------------------------");
		System.out.println();
	}

	/**
	 * Shows each individual recorded program
	 * 
	 * @param recordingsList List of all the programs that the user wants to record
	 */
	public static void showRecordings(ArrayList<Programme> recordingsList) {
		for (Programme prog : recordingsList) {
			System.out.println("|    " + prog.title + "   |   " + prog.channel + "  |    "
					+ String.format("%04d", prog.start) + "--" + String.format("%04d", prog.end) + "    |   "
					+ SimpleDateFormat.getDateInstance().format(prog.date) + "   |");
			System.out.println("|-------------------|---------------|------------|-------------|");
		}
	}

	/**
	 * The function which will display the EPG list for the selected day
	 * 
	 * @param tf             the format which time is held in
	 * @param userInput      The user's input
	 * @param channelList    The list of all the available channels
	 * @param recordingsList List of all the programs that the user wants to record
	 * @param tuners         the number of tuners which the user's system has
	 * @param df             the format the date is held in
	 * @param password       the password which the user has to ended if they want
	 *                       to record a program which starts after the watershed
	 */
	public static void EPGLayout(DateTimeFormatter tf, Scanner userInput, ArrayList<Channel> channelList,
			ArrayList<Programme> recordingsList, int tuners, DateTimeFormatter df, String password) {
		boolean leave = false;
		int currentlyOn = 0;
		int timeRightNow = 0;
		int dateToAdd = 0;
		while (!leave) {
			int plus = 0;
			int count = 0;
			System.out.println(
					"__________________________________________________________________________________________________________________________________________________________________________________________________________");
			System.out.println(
					"|                    |                                                           |                                                           |                                                           |");
			System.out.println("|      Channels      |                       " + timeAddition(timeRightNow, tf) + " - "
					+ timeAddition(timeRightNow + 1, tf) + "                         |                         "
					+ timeAddition(timeRightNow + 1, tf) + " - " + timeAddition(timeRightNow + 2, tf)
					+ "                       |                      " + timeAddition(timeRightNow + 2, tf) + " - "
					+ timeAddition(timeRightNow + 3, tf) + "                          |");
			System.out.println(
					"|____________________|___________________________________________________________|___________________________________________________________|___________________________________________________________|");
			while (count != 4) {
				if (outputChannel(channelList, currentlyOn + plus, timeRightNow, tf, df, dateToAdd)) {
					count++;
				}
				plus++;
			}
			System.out.println(
					"|      Page Up:      |                        Page Down:                         |                         Forward 3                         |                          Back 3                           |");
			System.out.println(
					"|         U          |                            D                              |                          hrs: F                           |                          hrs: B                           |");
			System.out.println(
					"|____________________|___________________________________________________________|___________________________________________________________|___________________________________________________________|");
			System.out.println(
					"|     Return to      |             Minus          |             Add              |                           Search EPG:                     |                     Now and Next                          |");
			System.out.println(
					"|      menu: R       |            Day: M          |           Day: A             |                                S                          |                         : N                               |");
			System.out.println(
					"----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			char choice = userInput.next().toUpperCase().charAt(0);
			switch (choice) {
			case 'U':
				if (currentlyOn == 0) {
					currentlyOn = 40;
				} else {
					currentlyOn -= 20;
				}
				break;
			case 'D':
				if (currentlyOn == 40) {
					currentlyOn = 0;
				} else {
					currentlyOn += 20;
				}
				break;
			case 'F':
				timeRightNow += 3;
				break;
			case 'B':
				timeRightNow -= 3;
				break;
			case 'S':
				searchEPG(channelList, userInput, recordingsList, tuners, password);
				break;
			case 'R':
				leave = true;
				break;
			case 'N':
				nowAndNext(channelList, tf, df);
				break;
			case 'M':
				dateToAdd -= 1;
				break;
			case 'A':
				dateToAdd += 1;
			}

		}
	}

	/**
	 * formats the 4 rows of channel data which will be displayed on the EPG
	 * 
	 * @param channelList  The list of all the available channels
	 * @param currentlyOn  The position which the display is currently on in the
	 *                     channel list
	 * @param timeRightNow The difference in time between the current time, and the
	 *                     current viewpoint start
	 * @param tf           The format that the time is held in
	 * @param df           The format that the date is held in
	 * @param dateToAdd    The number of days added or taken away from the current
	 *                     date
	 * @return Returns the formatted line
	 */
	public static boolean outputChannel(ArrayList<Channel> channelList, int currentlyOn, int timeRightNow,
			DateTimeFormatter tf, DateTimeFormatter df, int dateToAdd) {
		int channelSize = 20;
		int spaces;
		boolean valid = false;
		String spacesOtherSide = "";
		String spacesToAdd = "";
		String finalString = "|";
		Channel currentChannel = channelList.get(currentlyOn);
		if (LocalDate.now().plusDays(dateToAdd).format(df)
				.equals(SimpleDateFormat.getDateInstance().format(currentChannel.date))) {
			valid = true;
			int channelLength = currentChannel.id.length();
			spaces = (int) (Math.floor((channelSize - channelLength) / 2));
			for (int k = 0; k < spaces; k++) {
				spacesToAdd += " ";
			}
			for (int l = 0; l < ((channelSize - channelLength) - spaces); l++) {
				spacesOtherSide += " ";
			}
			finalString += spacesToAdd + currentChannel.id + spacesOtherSide + "|";
			spacesToAdd = "";
			spacesOtherSide = "";
			collectPrograms(currentChannel, timeRightNow, tf, finalString);
		}
		return valid;
	}

	/**
	 * Function used to add spaces to format a line
	 * 
	 * @param prog   The program which the function is adding to spaces to
	 * @param length The length of the box which the program will be stored in
	 * @return Returns the formatted program line
	 */
	public static String addSpaces(Programme prog, int length) {
		String spacesOtherSide = "";
		String spacesToAdd = "";
		String currentString = "";
		int spaces;
		boolean longEnough = false;
		String title = prog.title;
		while (longEnough == false) {
			if (length < title.length()) {
				title = title.substring(0, (title.length() / 2) - 4) + "...";
			} else {
				longEnough = true;
			}
		}
		spaces = (int) (Math.floor((length - title.length()) / 2));
		for (int k = 0; k < spaces; k++) {
			spacesToAdd += " ";
		}
		for (int l = 0; l < ((length - title.length()) - spaces); l++) {
			spacesOtherSide += " ";
		}
		currentString += spacesToAdd + title + spacesOtherSide + "|";
		spacesToAdd = "";
		spacesOtherSide = "";
		return currentString;
	}

	/**
	 * Collects all of the programs in the viewport window time scale for the 4
	 * displaying channels
	 * 
	 * @param currentChannel The current channel that is being displayed
	 * @param timeRightNow   The current time which the viewport is on
	 * @param tf             The format which the time is held in
	 * @param finalString    The formatted channel
	 */
	private static void collectPrograms(Channel currentChannel, int timeRightNow, DateTimeFormatter tf,
			String finalString) {
		int length = 0;
		int viewportSize = 3;
		String bottom = "|____________________|";
		LocalTime startViewPoint = getTimeOf(timeAddition(timeRightNow, tf));
		LocalTime endViewPoint = getTimeOf(timeAddition(timeRightNow + viewportSize, tf));
		for (Programme prog : currentChannel.programme) {
			LocalTime programStart = getTimeOf(String.format("%04d", prog.start));
			LocalTime programEnd = getTimeOf(String.format("%04d", prog.end));
			// works out if the program starts before the 'viewpoint' starts, but finishes
			// before the 'viewpoint' ends
			if (startViewPoint.isAfter(programStart) && startViewPoint.isBefore(programEnd)) {
				length = ((programEnd.toSecondOfDay() / 60) - (startViewPoint.toSecondOfDay() / 60)) - 1;
				finalString += addSpaces(prog, length);
				bottom += addBottom(length);
				// works out if the program starts before the 'viewpoint' ends, but ends after
				// the viewpoint ends
			} else if (endViewPoint.isAfter(programStart) && endViewPoint.isBefore(programEnd)) {
				length = ((endViewPoint.toSecondOfDay() / 60) - (programStart.toSecondOfDay() / 60)) - 1;
				finalString += addSpaces(prog, length);
				bottom += addBottom(length);
				// works out if the program starts ands ends in between the start and end of the
				// 'viewpoint'
			} else if (isContained(prog, startViewPoint, endViewPoint)) {
				length = ((programEnd.toSecondOfDay() / 60) - (programStart.toSecondOfDay() / 60)) - 1;
				finalString += addSpaces(prog, length);
				bottom += addBottom(length);
			}
			length = 0;
		}

		System.out.println(finalString);
		System.out.println(bottom);
	}

	/**
	 * Function which tests whether the program is contained within the viewpoint
	 * 
	 * @param prog           The program which is being checked
	 * @param startViewPoint Where the viewpoint begins
	 * @param endViewPoint   Where the viewpoint finishes
	 * @return Returns whether the program is contained
	 */
	public static boolean isContained(Programme prog, LocalTime startViewPoint, LocalTime endViewPoint) {
		LocalTime programStart = getTimeOf(String.format("%04d", prog.start));
		LocalTime programEnd = getTimeOf(String.format("%04d", prog.end));

		boolean startOk = (programStart.isAfter(startViewPoint) || programStart.equals(startViewPoint))
				&& programStart.isBefore(endViewPoint);
		boolean end = programEnd.isAfter(startViewPoint)
				&& (programEnd.isBefore(endViewPoint) || programEnd.equals(endViewPoint));

		return startOk && end;
	}

	/**
	 * Adds the divider between the displaying programs
	 * 
	 * @param length The length of the program which this will be the bottom for
	 * @return Returns the line
	 */
	public static String addBottom(int length) {
		String theBottom = "";
		for (int i = 0; i < length; i++) {
			theBottom += "_";
		}
		theBottom += "|";
		return theBottom;
	}

	/**
	 * Gets and converts the given time from a string
	 * 
	 * @param timeData The string which will be converted into a time
	 * @return Returns the newly formatted time
	 */
	public static LocalTime getTimeOf(String timeData) {
		return LocalTime.of(Integer.parseInt(timeData.substring(0, 2)), Integer.parseInt(timeData.substring(2, 4)));
	}

	/**
	 * Function used to display the viewport
	 * 
	 * @param plus Number of hours to be added to the current time
	 * @param tf   The format which the time is held in
	 * @return Returns the truncated time
	 */
	public static String timeAddition(int plus, DateTimeFormatter tf) {
		return LocalTime.now().truncatedTo(ChronoUnit.HOURS).plusHours(plus).format(tf).toString();
	}

	/**
	 * Displays now and next for every channel in the list
	 * 
	 * @param channelList List of all the channels
	 * @param tf          The format which the time is held in
	 * @param df          The format that the date is held in
	 */
	public static void nowAndNext(ArrayList<Channel> channelList, DateTimeFormatter tf, DateTimeFormatter df) {
		int time = Integer.parseInt(LocalTime.now().format(tf).toString());
		Programme nowProg = null;
		for (Channel chan : channelList) {
			for (Programme prog : chan.programme) {
				if (LocalDate.now().format(df).equals(SimpleDateFormat.getDateInstance().format(chan.date))) {
					boolean now = (prog.start <= time) && (prog.end > time);
					boolean next = false;
					if (nowProg != null) {
						int duration = nowProg.end - nowProg.start;
						next = ((prog.start <= (time + (duration))) && (prog.end >= time + (duration)));
					}
					if (now) {
						nowProg = prog;
					}
					if (now || next) {
						System.out.print((now ? "now" : next ? "next" : "eek") + " ");
						System.out.println(SimpleDateFormat.getDateInstance().format(chan.date) + " " + chan.id + " "
								+ String.format("%04d", prog.start) + "--" + String.format("%04d", prog.end) + " -- "
								+ prog.title);
					}
				}
			}
		}
	}

	/**
	 * Writes the recordings into a schedule text file
	 * @param recordingsList List of all of the user's recorded programs
	 * @throws IOException Exception for the file
	 */
	public static void writeRecordings(ArrayList<Programme> recordingsList) throws IOException {
		for (Programme prog : recordingsList) {
			System.out.println(prog.title);
		}
		FileWriter writeToFile;
		String fileName = "schedules.txt";
		try {
			writeToFile = new FileWriter(fileName);
			for (Programme prog : recordingsList) {
				writeToFile.write(prog.title + "\n");
				writeToFile.write(prog.channel + "\n");
				writeToFile.write(String.format("%04d", prog.start) + "\n");
				writeToFile.write(String.format("%04d", prog.end) + "\n");
				writeToFile.write(SimpleDateFormat.getDateInstance().format(prog.date) + "\n");
			}
			writeToFile.close();
			System.out.println("Written file with" + recordingsList.size());
			recordingsList.clear();
		} catch (IOException e) {
			System.out.println("Could not find the file " + e.getMessage());
		}
	}

	/**
	 * Collects all of the recordings which are in the schedule text file, and adds it to the recordings list
	 * @param recordingsList List of all of the recorded programs
	 * @param channelList List of all the channels
	 * @param tunersLeft Number of tuner which the user's system has
	 */
	public static void collectRecordings(ArrayList<Programme> recordingsList, ArrayList<Channel> channelList,
			int tunersLeft) {
		FileReader file;
		try {
			file = new FileReader("schedules.txt");
			Scanner read = new Scanner(file);
			while (read.hasNext()) {
				String title = read.nextLine();
				String channel = read.nextLine();
				String start = read.nextLine();
				String end = read.nextLine();
				String date = read.nextLine();
				for (Channel chan : channelList) {
					for (Programme prog : chan.programme) {
						if (prog.title.equals(title) && prog.channel.equals(channel)
								&& String.format("%04d", prog.start).equals(start)
								&& String.format("%04d", prog.end).equals(end)
								&& SimpleDateFormat.getDateInstance().format(prog.date).toString().equals(date)) {
							if (recordingClash(prog, tunersLeft, recordingsList)) {
								tunersLeft -= 1;
							}
							recordingsList.add(prog);
						}
					}
				}
			}
			read.close();
		} catch (FileNotFoundException e) {
			System.out.println("Could not find the file " + e.getMessage());
		}
	}
}
