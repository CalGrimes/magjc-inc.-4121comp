import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class XMLDataFormatter extends XmlAdapter<String, Date> {

	private static final String CUSTOM_FORMAT_STRING = "dd/MM/yyyy";

	@Override
	public String marshal(Date v) {
		return new SimpleDateFormat(CUSTOM_FORMAT_STRING).format(v);
	}

	@Override
	public Date unmarshal(String v) {
		try {
			return new SimpleDateFormat(CUSTOM_FORMAT_STRING).parse(v);
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

}
